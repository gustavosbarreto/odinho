QT -= gui
QT += sql

SOURCES += main.cpp

ODIN_QT_PROJECTS = repository

for ( PROJECT, ODIN_QT_PROJECTS ) {
    LIBS += -L$$ODIN_DIR/odin-qt-$${PROJECT}/$$DESTDIR -lodin-qt-$${PROJECT}

    INCLUDEPATH += $$ODIN_DIR/odin-qt-$${PROJECT}
    QMAKE_RPATHDIR += $$ODIN_DIR/odin-qt-$${PROJECT}/$$DESTDIR
}

INCLUDEPATH += $$ODIN_DIR
