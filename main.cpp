#include <QCoreApplication>
#include <QSqlError>
#include <QDebug>

#include <odin-qt-repository/odinqsqlquery.h>
#include <odin-qt-repository/odinqsqldatabase.h>

using namespace odin::repository;

int main(int argc, char *argv[]) {
    QCoreApplication app(argc, argv);

    OdinQSqlQuery().exec("SELECT current_database()");

    qDebug() << OdinQSqlDatabase::instance().getConnection()->lastError();
    
    return app.exec();
}
